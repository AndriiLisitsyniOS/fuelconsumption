//
//  TableViewCell.swift
//  fuelConsumption
//
//  Created by andrey on 5/4/18.
//  Copyright © 2018 waygem. All rights reserved.
//

import UIKit

class TableViewCell: UITableViewCell {

    @IBOutlet weak var fuelingNumLbl: UILabel!
    @IBOutlet weak var dateLbl: UILabel!
    @IBOutlet weak var consumptionLbl: UILabel!
    @IBOutlet weak var consumptionNameLbl: UILabel!

    func configureCell(fueling: Fueling, consumption: Double, indexPathRow: Int) {
        fuelingNumLbl.text = "\(indexPathRow + 1))"
        dateLbl.text = fueling.date
        if consumption == 0 {
            consumptionLbl.text = " —"
        } else {
            consumptionLbl.text = String(Double(round(10*consumption)/10))
        }
    }

}
