//
//  ViewController.swift
//  fuelConsumption
//
//  Created by andrey on 5/2/18.
//  Copyright © 2018 waygem. All rights reserved.
//

import UIKit
import CoreData

let appDelegate = UIApplication.shared.delegate as? AppDelegate

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var consumptionLbl: UILabel!
    //@IBOutlet weak var bannerView: GADBannerView!
    @IBOutlet weak var swipeView: UIView!
    @IBOutlet weak var swipeImg: UIImageView!
    @IBOutlet weak var swipeLbl: UILabel!
    @IBOutlet weak var okBtn: UIButton!
    var fuelings: [Fueling] = []
    var fuelingsDetails: [FuelingDetails] = []
    var lastConsumptions: [LastConsumption] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        //bannerView.adUnitID = "ca-app-pub-2220353646614349/1805794742"
        //bannerView.rootViewController = self
        //let request = GADRequest()
        //request.testDevices = ["b5b36ccd53370e45f0f66a6aa0e0c447", "c2fc9ae806b3e1b2a539dbffb5b85f4b"]
        //bannerView.load(request)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        fetchCoreDataObjects()
        fillFuelingDetails()
        countConsumptions()
        changeConsumptionLbl()
        tableView.reloadData()
        if UserDefaults.standard.bool(forKey: "swipeInstructionsShown") == false && fuelings.count != 0 {
            self.swipeView.isHidden = false
            self.swipeImg.isHidden = false
            self.swipeLbl.isHidden = false
            self.okBtn.isHidden = false
            UIView.animate(withDuration: 2, delay: 0, options: .repeat, animations: {
                self.swipeImg.center.x -= self.view.bounds.width
            }, completion: nil)
            UserDefaults.standard.set(true, forKey: "swipeInstructionsShown")
        }
        let saveCount = UserDefaults.standard.integer(forKey: "saveCount")
        let showReview = UserDefaults.standard.bool(forKey: "showReview")
        let saveNumToShowReview = 3
        guard saveCount != 0 && saveCount % saveNumToShowReview == 0 && showReview else { return }
        askReview()
        UserDefaults.standard.set(false, forKey: "showReview")
    }
    
    func fetchCoreDataObjects() {
        self.fetch { (complete) in
            if complete {
                if fuelings.count >= 1 {
                    tableView.isHidden = false
                } else {
                    tableView.isHidden = true
                }
            }
        }
    }
    
    func fillFuelingDetails() {
        fuelingsDetails = [FuelingDetails]()
        for f in fuelings {
            let fuelingDetails = FuelingDetails()
            fuelingDetails.configureDetails(fueling: f)
            fuelingsDetails.append(fuelingDetails)
        }
    }
    
    func countConsumptions() {
        lastConsumptions = [LastConsumption]()
        guard fuelingsDetails.count != 0 else { return }
        for fuelingNum in 0...fuelingsDetails.count - 1 {
            let consumption = LastConsumption()
            consumption.countLastConsumption(fuelings: fuelingsDetails, fuelingNum: fuelingNum)
            lastConsumptions.append(consumption)
        }
    }
    
    func changeConsumptionLbl() {
        if fuelingsDetails.count > 1 {
            let run = (fuelingsDetails.last?.run)! - fuelingsDetails[0].run
            var litres = 0
            for l in fuelingsDetails {
                litres += l.litres
            }            
            if (fuelingsDetails.first?.fullTank)! && (fuelingsDetails.last?.fullTank)! {
                litres = litres - fuelingsDetails[0].litres
            } else {
                litres = litres - (fuelingsDetails.last?.litres)!
            }
            let consumption = Double(litres)/Double(run) * 100
            consumptionLbl.text = run != 0 ? String(Double(round(10*consumption)/10)) : "0"
        } else {
            consumptionLbl.text = "0"
        }
    }
    
    func fetch(completion: (_ complete: Bool) -> ()) {
        guard let managedContext = appDelegate?.persistentContainer.viewContext else { return }
        let fetchRequest = NSFetchRequest<Fueling>(entityName: "Fueling")
        do {
            fuelings = try managedContext.fetch(fetchRequest)
            print("Successfully fetched data.")
            completion(true)
        } catch {
            debugPrint("Could not fetch: \(error.localizedDescription)")
            completion(false)
        }
    }
    
    func askReview() {
        if #available(iOS 10.3, *){
            let appDelegate = AppDelegate()
            appDelegate.requestReview()
        } else if UserDefaults.standard.bool(forKey: "rated") != true {
            self.showAskReviewAlert()
        }
    }
    
    func showAskReviewAlert() {
        let alert = UIAlertController(title: "Оцените Нас", message:"Если Вам\n нравится приложение, \nпож-та оцените его.", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Оценить", style: .cancel) { _ in
            UIApplication.shared.open(URL(string : "itms-apps://itunes.apple.com/app/id1381215006")!, options: [:], completionHandler: nil)
            UserDefaults.standard.set(true, forKey: "rated")
        })
        alert.addAction(UIAlertAction(title: "Не Сейчас", style: .default) { _ in
        })
        self.present(alert, animated: true){}
    }
    
    @IBAction func okBtnPressed(_ sender: Any) {
        hideSwipeInstructions()
    }
    
    func hideSwipeInstructions() {
        let swipeInstructionsArray = [swipeView, swipeImg, swipeLbl, okBtn]
        UIView.animate(withDuration: 0.4, delay: 0, options: .curveEaseOut,
                       animations: {
                        for s in swipeInstructionsArray {
                            s?.alpha = 0
                        }
        },
                       completion: { _ in
                        for s in swipeInstructionsArray {
                            s?.isHidden = true
                        }
        })
    }

    // MARK: UITableViewDelegate, UITableViewDataSource
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return fuelings.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "tableViewCell") as? TableViewCell else { return UITableViewCell() }
        let fueiling = fuelings[indexPath.row]
        let consumption = lastConsumptions[indexPath.row].lastConsumption
        cell.configureCell(fueling: fueiling, consumption: consumption, indexPathRow: indexPath.row)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: "changeDetails", sender: indexPath.row)
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCellEditingStyle {
        return .none
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch UIDevice.current.userInterfaceIdiom {
        case .phone:
            return 70
        default:
            return 105
        }
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        let deleteAction = UITableViewRowAction(style: .destructive, title: "УДАЛИТЬ") { (rowAction, indexPath) in
            self.removeFueling(adIndexPath: indexPath)
            self.fetchCoreDataObjects()
            self.fillFuelingDetails()
            self.countConsumptions()
            tableView.deleteRows(at: [indexPath], with: .automatic)
            Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.reloadTableView), userInfo: nil, repeats: false)
            self.changeConsumptionLbl()
        }
        deleteAction.backgroundColor = #colorLiteral(red: 1, green: 0.1491314173, blue: 0, alpha: 1)
        return [deleteAction]
    }
    
    @objc func reloadTableView() {
        tableView.reloadData()
    }
    
    func removeFueling(adIndexPath indexPath: IndexPath) {
        guard let managedContext = appDelegate?.persistentContainer.viewContext else { return }
        managedContext.delete(fuelings[indexPath.row])
        
        do {
            try managedContext.save()
            print("Successfully remmoved fueiling!")
        } catch {
            debugPrint("Could not remove: \(error.localizedDescription)")
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let detailsVC = segue.destination as? DetailsVC
        if segue.identifier == "changeDetails" {
            detailsVC?.currentFueling = fuelingsDetails[sender as! Int]
            detailsVC?.fuelingNum = sender as! Int
            detailsVC?.changeDetails = true
        }
    }
}






