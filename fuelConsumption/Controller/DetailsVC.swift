//
//  DetailsVC.swift
//  fuelConsumption
//
//  Created by andrey on 5/4/18.
//  Copyright © 2018 waygem. All rights reserved.
//

import UIKit
import CoreData

class DetailsVC: UIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var litersTextField: UITextField!
    @IBOutlet weak var runTextField: UITextField!
    @IBOutlet weak var tankSwitch: UISwitch!
    //@IBOutlet weak var bannerView: GADBannerView!
    var fullTank = false
    var currentFueling = FuelingDetails()
    var fuelingNum = 0
    var changeDetails = false
    var fuelings = [FuelingDetails]()
    var saveCount = UserDefaults.standard.integer(forKey: "saveCount")

    override func viewDidLoad() {
        super.viewDidLoad()
        runTextField.delegate = self
        litersTextField.delegate = self
        //bannerView.adUnitID = "ca-app-pub-2220353646614349/1805794742"
        //bannerView.rootViewController = self
        //let request = GADRequest()
        //request.testDevices = ["b5b36ccd53370e45f0f66a6aa0e0c447", "c2fc9ae806b3e1b2a539dbffb5b85f4b"]
        //bannerView.load(request)
        guard changeDetails else { return }
        litersTextField.text = "\(currentFueling.litres)"
        runTextField.text = "\(currentFueling.run)"
        tankSwitch.isOn = currentFueling.fullTank
        fullTank = currentFueling.fullTank
    }

    @IBAction func tankSwitchPressed(_ sender: Any) {
        fullTank = !fullTank
    }

    @IBAction func saveBtnPressed(_ sender: Any) {
        guard litersTextField.text != "" && runTextField.text != "" else { return }
        if changeDetails {
            edit { (complete) in
                if complete {
                    dismissVC()
                }
            }
        } else {
            save { (complete) in
                if complete {
                    dismissVC()
                    saveCount += 1
                    UserDefaults.standard.set(saveCount, forKey: "saveCount")
                    UserDefaults.standard.set(true, forKey: "showReview")
                }
            }
        }
    }
    
    func dismissVC() {
        view.endEditing(true)
        dismiss(animated: true, completion: nil)
    }
    
    func save(completion: (_ finished: Bool) -> ()) {
        guard let managedContext = appDelegate?.persistentContainer.viewContext else { return }
        let fueling = Fueling(context: managedContext)
        let dateFormater = DateFormatter()
        dateFormater.dateFormat = "dd.MM.yyyy HH:mm"
        let date = dateFormater.string(from: Date())
        fueling.date = date
        fueling.fullTank = fullTank
        fueling.run = Int32(runTextField.text!)!
        fueling.liters = Int32(litersTextField.text!)!
        do {
            try managedContext.save()
            print("Success")
            completion(true)
        } catch {
            debugPrint("Could not save: \(error.localizedDescription)")
            completion(false)
        }
    }
    
    func edit(completion: (_ finished: Bool) -> ()) {
        guard let managedContext = appDelegate?.persistentContainer.viewContext else { return }
        let fetchRequest = NSFetchRequest<Fueling>(entityName: "Fueling")
        do {
            let results = try managedContext.fetch(fetchRequest)
            results[fuelingNum].setValue(Int32(litersTextField.text!)!, forKey: "liters")
            results[fuelingNum].setValue(Int32(runTextField.text!)!, forKey: "run")
            results[fuelingNum].setValue(fullTank, forKey: "fullTank")
        } catch {
            print("Fetch Failed: \(error)")
        }
        do {
            try managedContext.save()
            print("Success")
            completion(true)
        } catch {
            debugPrint("Could not save: \(error.localizedDescription)")
            completion(false)
        }
    }

    @IBAction func backBtnPressed(_ sender: Any) {
        dismissVC()
    }
    
}
