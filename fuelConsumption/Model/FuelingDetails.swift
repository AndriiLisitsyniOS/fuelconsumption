//
//  FuelingDetails.swift
//  fuelConsumption
//
//  Created by andrey on 9/21/18.
//  Copyright © 2018 waygem. All rights reserved.
//

import Foundation

class FuelingDetails {
    var date = ""
    var fullTank = false
    var litres = 0
    var run = 0
    
    func configureDetails(fueling: Fueling) {
        date = fueling.date!
        fullTank = fueling.fullTank
        litres = Int(fueling.liters)
        run = Int(fueling.run)
    }
}
