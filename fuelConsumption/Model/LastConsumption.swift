//
//  LastConsumption.swift
//  fuelConsumption
//
//  Created by andrey on 9/24/18.
//  Copyright © 2018 waygem. All rights reserved.
//

import Foundation

class LastConsumption {
    var lastConsumption = 0.0
    
    func countLastConsumption(fuelings: [FuelingDetails], fuelingNum: Int) {
        guard fuelings.count > 1 else { return }
        guard fuelingNum != 0 else { return }
        let run = fuelings[fuelingNum].run - fuelings[fuelingNum-1].run
        guard run != 0 else { return }
        var litres = 0
        if fuelings[fuelingNum-1].fullTank && fuelings[fuelingNum].fullTank {
            litres = fuelings[fuelingNum].litres
        } else {
            litres = fuelings[fuelingNum-1].litres
        }
        lastConsumption = Double(litres)/Double(run) * 100
    }
}
